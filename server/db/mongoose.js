const mongoose = require('mongoose')

// Set Mongoose promises
mongoose.Promise = global.Promise

let connectionUri = process.env.MONGODB_URI

// Setup connection as the new version requires it
let mongoConn = mongoose.connect(connectionUri, {
  useMongoClient: true
}).then(db => {
  console.log('MongoDB is connected')
}).catch(error => {
  console.log(error)
})

module.exports.mongoose = mongoose
