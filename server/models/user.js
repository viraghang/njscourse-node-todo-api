const mongoose = require('mongoose')
const validator = require('validator')
const jwt = require('jsonwebtoken')
const _ = require('lodash')
const bcrypt = require('bcryptjs')

let userSchema = new mongoose.Schema({
  email: {
    trim: true,
    required: true,
    type: String,
    minlength: 1,
    unique: true,
    validate: {
      isAsync: false,
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email'
    }
  },
  password: {
    type: String,
    require: true,
    minlength: 6
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }],
  todos: [
    {
      type: mongoose.Schema.Types.ObjectId, ref: 'Todo'
    }
  ]
})

// Instance methods
userSchema.methods.toJSON = function () {
  let user = this // instance
  let userObject = user.toObject()

  return _.pick(userObject, ['_id', 'email'])
}

userSchema.methods.generateAuthToken = function () {
  let user = this
  let access = 'auth'
  let token = jwt.sign({_id: user._id.toHexString(), access}, process.env.JWT_SECRET).toString()
  user.tokens.push({access, token})
  return user.save().then(() => token)
}

userSchema.methods.removeToken = function (token) {
  let user = this
  return user.update({
    $pull: {
      tokens: {token}
    }
  })
}

// Model methods
userSchema.statics.findByToken = function (token) {
  let User = this // Model
  let decoded
  try {
    decoded = jwt.verify(token, process.env.JWT_SECRET)
  } catch (e) {
    // return new Promise((resolve, reject) => reject()) equals to
    return Promise.reject()
  }

  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  })
}

userSchema.statics.findByCredentials = function (email, password) {
  let User = this
  return User.findOne({email}).then(user => {
    if (!user) return Promise.reject()
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (err, result) => {
        if (err || !result) reject()
        resolve(user)
      })
    })
  })
}

// Mongoose middleware
userSchema.pre('save', function (next) {
  let user = this
  if (user.isModified('password') || user.isNew) {
    let pass = user.password
    bcrypt.genSalt(10, (err, salt) => {
      if (err) next(new Error('Processing error'))
      bcrypt.hash(pass, salt, (err, hash) => {
        if (err) next(new Error('Processing error'))
        user.password = hash
        next()
      })
    })
  } else {
    next()
  }
})

module.exports = mongoose.model('User', userSchema)
