const express = require('express')
const {ObjectID} = require('mongodb')
const _ = require('lodash')

const todoRouter = express.Router()
const Todo = require('../models/todo')
// const User = require('../models/user') // Added for populate experiment
const authenticate = require('./../middleware/authenticate')

todoRouter
  .post('/', authenticate, (req, res) => {
    let todo = new Todo({
      text: req.body.text
    })
    todo._creator = req.user
    todo.save().then(doc => {
      req.user.todos.push(doc)
      req.user.save()
    }).then(() => {
      res.status(200).send(todo)
    }).catch(e => {
      res.status(400).send(e)
    })
  })

  .get('/', authenticate, (req, res) => {
    Todo.find({_creator: req.user._id}).then(todos => {
      res.status(200).send({todos})
    }, e => {
      res.status(400).send(e)
    })

    // MY EXPERIMENT WITH MONGOOSE POPULATE
    // if (req.user.todos) {
    //   User.findById(req.user._id).populate('todos').exec((err, person) => {
    //     if (err) res.status(400).send(err)
    //     res.status(200).send({todos: person.todos})
    //   })
    // } else {
    //   res.status(404).send()
    // }
  })

  .get('/:id', authenticate, (req, res) => {
    let id = req.params.id
    if (!ObjectID.isValid(id)) res.status(400).send()
    Todo.findOne({
      _id: id,
      _creator: req.user._id
    }).then(todo => {
      if (!todo) res.status(404).send()
      res.status(200).send({todo})
    }).catch(e => {
      res.status(400).send()
    })
  })

  .delete('/:id', authenticate, (req, res) => {
    let id = req.params.id
    if (!ObjectID.isValid(id)) res.status(400).send()
    Todo.findOneAndRemove({
      _id: id,
      _creator: req.user._id
    }).then(todo => {
      if (!todo) res.status(404).send()
      res.status(200).send({todo})
    }).catch(e => res.status(400).send())
  })

  .patch('/:id', authenticate, (req, res) => {
    let id = req.params.id
    let body = _.pick(req.body, ['text', 'completed'])

    if (!ObjectID.isValid(id)) res.status(400).send()

    if (_.isBoolean(body.completed) && body.completed) {
      body.completedAt = new Date().getTime()
    } else {
      body.completed = false
      body.completedAt = null
    }

    Todo.findOneAndUpdate({_id: id, _creator: req.user._id}, {$set: body}, {new: true}).then(todo => {
      if (!todo) res.status(404).send()
      res.send({todo})
    }).catch(e => res.status(400).send())
  })

module.exports = todoRouter
