const express = require('express')
const {ObjectID} = require('mongodb')
const _ = require('lodash')

const userRouter = express.Router()
const User = require('../models/user')

const authenticate = require('./../middleware/authenticate')

userRouter
  .get('/', (req, res) => {
    User.find().then(users => {
      res.status(200).send({users})
    }, e => {
      res.status(400).send(e)
    })
  })

  .get('/me', authenticate, (req, res) => {
    res.send(req.user)
  })

  .post('/', (req, res) => {
    let body = _.pick(req.body, ['email', 'password'])
    let user = new User(body)
    user
      .save()
      .then(() => user.generateAuthToken())
      .then(token => res.header('x-auth', token).status(200).send(user))
      .catch(e => res.status(400).send(e))
  })

  .post('/login', (req, res) => {
    let body = _.pick(req.body, ['email', 'password'])
    User
      .findByCredentials(body.email, body.password)
      .then(user => {
        user.generateAuthToken().then(token => {
          res.header('x-auth', token).status(200).send(user)
        })
      })
      .catch(e => res.status(400).send())
  })

  .delete('/me/token', authenticate, (req, res) => {
    req.user.removeToken(req.token).then(() => {
      res.status(200).send()
    }, () => {
      res.status(400).send()
    })
  })

  .delete('/:id', (req, res) => {
    let id = req.params.id
    if (!ObjectID.isValid(id)) res.status(400).send()
    User.findByIdAndRemove(id).then(user => {
      if (!user) res.status(404).send()
      res.status(200).send({user})
    }).catch(e => res.status(400).send())
  })

  // .patch('/:id', (req, res) => {
  //   let id = req.params.id
  //   let body = _.pick(req.body, ['text', 'completed'])
  //
  //   if (!ObjectID.isValid(id)) res.status(400).send()
  //
  //   if (_.isBoolean(body.completed) && body.completed) {
  //     body.completedAt = new Date().getTime()
  //   } else {
  //     body.completed = false
  //     body.completedAt = null
  //   }
  //
  //   User.findByIdAndUpdate(id, {$set: body}, {new: true}).then(todo => {
  //     if (!todo) res.status(404).send()
  //     res.send({todo})
  //   }).catch(e => res.status(400).send())
  // })

module.exports = userRouter
