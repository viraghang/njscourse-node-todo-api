require('./config/config.js')
const port = process.env.PORT
const {mongoose} = require('./db/mongoose')

const express = require('express')
const bodyParser = require('body-parser')

let app = express()
let todoRouter = require('./routes/todo-router')
let userRouter = require('./routes/user-router')

app.use(bodyParser.json())
app.use('/todos', todoRouter)
app.use('/users', userRouter)

app.listen(port, () => console.log(`Started on port ${port}`))

module.exports = app
