// const MongoClient = require('mongodb').MongoClient
const {MongoClient, ObjectID} = require('mongodb') // Basic Destructuring

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) return console.log('Unable to connect to DB server', err)
  console.log('Connected to MongoDB server')

  // db.collection('Todos').find({_id: new ObjectID('59665bd4a7dc35565c771e07')}).toArray().then(docs => {
  //   console.log('Todos')
  //   console.log(JSON.stringify(docs, null, 2))
  // }, err => {
  //   console.log('Unable to fetch the documents', err)
  // })

  // db.collection('Todos').find().count().then(count => {
  //   console.log(`Todos count: ${count}`)
  // }, err => {
  //   console.log('Unable to fetch the documents', err)
  // })

  db.collection('Users').find({name: 'Zoltán Virág'}).toArray().then(docs => {
    console.log('Users')
    console.log(JSON.stringify(docs, null, 2))
  }, err => {
    console.log('Unable to fetch the documents', err)
  })

  // db.close()
})
