const bcrypt = require('bcryptjs')

let pass = '123abc!'

// bcrypt.genSalt(10, (err, salt) => {
//   bcrypt.hash(pass, salt, (err, hash) => {
//     console.log(hash)
//   })
// })

let hashedPass = '$2a$10$2PU6COgmWddqpG.ZalHxSe1z3DboDXvhW1jw5lD6FdHd1x5Xg/Uoa'

bcrypt.compare(pass, hashedPass, (err, res) => {
  if (err) console.log(err)
  console.log(res)
})

// const {SHA256} = require('crypto-js')
// const jwt = require('jsonwebtoken')
//
// let data = {
//   id: 10
// }
//
// let token = jwt.sign(data, '123abc')
// console.log(token)
//
// let decoded = jwt.verify(token, '123abc')
//
// console.log('decoded', decoded)

// let message = 'I am user number 3'
// let hash = SHA256(message).toString()
//
// console.log(`Message: ${message}`)
// console.log(`Hash: ${hash}`)
//
// let data = {
//   id: 4
// }
//
// let token = {
//   data,
//   hash: SHA256(JSON.stringify(data) + 'somesecret').toString()
// }
//
// token.data.id = 5
// token.hash = SHA256(JSON.stringify(token.data)).toString()
//
// let resultHash = SHA256(JSON.stringify(data) + 'somesecret').toString()
//
// if (resultHash === token.hash) {
//   console.log('Data was not changes')
// } else {
//   console.log('Data was changed, do not trust')
// }
