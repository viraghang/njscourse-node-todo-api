// const MongoClient = require('mongodb').MongoClient
const {MongoClient, ObjectID} = require('mongodb') // Basic Destructuring

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) return console.log('Unable to connect to DB server', err)
  console.log('Connected to MongoDB server')

  // db.collection('Todos').insertOne({
  //   text: 'Something to do',
  //   completed: false
  // }, (err, res) => {
  //   if (err) return console.log('Unable to insert todo', err)
  //   console.log(JSON.stringify(res.ops, null, 2))
  // })

  // db.collection('Users').insertOne({
  //   name: 'Anita Schreiter',
  //   age: 36,
  //   location: '1125 Budapest, Szent Orbán tér 99.'
  // }, (err, res) => {
  //   if (err) return console.log('Unable to insert user', err)
  //   // console.log(JSON.stringify(res.ops, null, 2))
  //   console.log(res.ops[0]._id.getTimestamp())
  // })

  db.close()
})
