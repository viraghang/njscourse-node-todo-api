const {ObjectID} = require('mongodb')

const {mongoose} = require('./../server/db/mongoose')
const Todo = require('./../server/models/todo')
const User = require('./../server/models/user')

let userId = '5967989c7c228c4364f1af81'

User.findById(userId).then(user => {
  if (!user) return console.log('User not found')
  console.log('User by ID:', user)
}).catch(e => console.log(e))

// let id = '5967c342b81a086d94e7e392'

// if (!ObjectID.isValid(id)) console.log('ID not valid')

// Todo.find({
//   _id: id // mongoose converts this to an ObjectID automatically
// }).then(todos => {
//   console.log('Todos', todos)
// })
//
// Todo.findOne({
//   _id: id
// }).then(todo => {
//   console.log('Todo', todo)
// })

// Todo.findById(id).then(todo => {
//   if (!todo) return console.log('ID not found')
//   console.log('Todo by ID', todo)
// }).catch(e => console.log(e))
