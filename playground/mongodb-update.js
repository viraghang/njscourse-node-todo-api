// const MongoClient = require('mongodb').MongoClient
const {MongoClient, ObjectID} = require('mongodb') // Basic Destructuring

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) return console.log('Unable to connect to DB server', err)
  console.log('Connected to MongoDB server')

  // db.collection('Todos').findOneAndUpdate({
  //   _id: new ObjectID('596744a63f3eafa0dc82c4fb')
  // }, {
  //   $set: {
  //     completed: true
  //   }
  // }, {
  //   returnOriginal: false
  // }).then(res => {
  //   console.log(JSON.stringify(res, null, 2))
  // }, err => {
  //   if (err) return console.log(err)
  // })

  db.collection('Users').findOneAndUpdate({
    _id: new ObjectID('59672394c6f39534802820fd')
  }, {
    $set: {
      name: 'Zoltán Virág'
    },
    $inc: {
      age: 1
    }
  }, {
    returnOriginal: false
  }).then(res => {
    console.log(JSON.stringify(res, null, 2))
  }, err => {
    if (err) return console.log(err)
  })

  // db.close()
})
